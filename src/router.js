import { createRouter, createWebHistory } from "vue-router";
import store from './store/index.js';

import CoachesList from "./pages/coaches/CoachesList.vue";

const ContactCoach = () => import('./pages/requests/ContactCoach.vue');
const CoachDetail = () => import('./pages/coaches/CoachDetail.vue');
const CoachRegister = () => import ('./pages/coaches/CoachRegister.vue');
const RequestsReceived = () => import('./pages/requests/RequestsReceived.vue');
const UserAuth = () => import('./pages/auth/UserAuth.vue');
const NotFound = () => import('./pages/NotFound.vue');

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", redirect: "/coaches" },
    { path: "/coaches", component: CoachesList },
    {
      path: "/coaches/:id",
      component: CoachDetail,
      props: true,
      children: [{ path: "/coaches/:id/contact", component: ContactCoach }],
    },
    { path: "/register", component: CoachRegister, meta: {requiresAuth: true} },
    { path: "/requests", component: RequestsReceived, meta: {requiresAuth: true}},
    { path: "/auth", component: UserAuth, meta: {requiresUnauth: true}},
    { path: "/:notFound(.*)", component: NotFound },
  ],
});

router.beforeEach(function(to, _, next){
  if(to.meta.requiresAuth && !store.getters.isAuthenticated){
    next('/auth');
  } else if (to.meta.requiresUnauth && store.getters.isAuthenticated){
    next('/coaches');
  } else {
    next();
  }
});

export default router;
