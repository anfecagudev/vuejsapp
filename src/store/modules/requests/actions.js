export default {
  async contactCoach(context, payload) {
    const newRequest = {
      coachId: payload.coachId,
      userEmail: payload.email,
      message: payload.message,
    };
    const response = await fetch(
      `https://coaches-project-86875-default-rtdb.firebaseio.com/requests/${payload.coachId}.json`,
      {
        method: "POST",
        body: JSON.stringify(newRequest),
      }
    );
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(
        responseData.message || "Failed to send request."
      );
      throw error;
    }
    newRequest.id = responseData.name;
    context.commit("addRequest", newRequest);
  },
  async fecthRequests(context) {
    const coachId = context.rootGetters.userId;
    const token = context.rootGetters.token;
    const response = await fetch(
      `https://coaches-project-86875-default-rtdb.firebaseio.com/requests.json?auth=${token}`
    );
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(
        responseData.message || "Failed to fetch requests"
      );
      throw error;
    }
    const requests = [];
    for (const key in responseData) {
      for (const innerkey in responseData[key]) {
        const item = responseData[key];
        const request = {
          id: coachId,
          coachId: key,
          userEmail: item[innerkey].userEmail,
          message: item[innerkey].message,
        };
        requests.push(request);
      }
    }
    context.commit("setRequests", requests);
  },
};
